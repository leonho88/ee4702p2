package effects
{
	public class PitchShifter
	{
		protected var source:Vector.<Number>; 
		protected var currentIndex:Number = 0; 
		protected var dest:Vector.<Number>;
		public var pitch:Number = 3; 
		
		/*
		public function PitchShifter(monoSample:Vector.<Number>) 
		{ 
			source = monoSample;
			dest = new Vector.<Number>(source.length*pitch);
		}*/
		
		public function setSource(monoSample:Vector.<Number>):void 
		{ 
			source = monoSample; 
			currentIndex = 0;
			dest = new Vector.<Number>(int(source.length*(1/pitch)));
		} 
		
		public function process(monoDestination:Vector.<Number>):void  
		{ 
			var numSamples:int = monoDestination.length; 
			
			var sourceLength:int = source.length-1; 
			
			for (var i:int = 0; i<numSamples; i++)  
			{ 
				currentIndex += pitch; 
				if (currentIndex >= sourceLength) currentIndex -= sourceLength; 
				
				var index1:int = int(currentIndex); 
				var index2:int = index1+1; 
				var index2Factor:Number = currentIndex-index1; 
				var index1Factor:Number = 1-index2Factor; 
				
				monoDestination[i] = source[index1]*index1Factor + source[index2]*index2Factor;
			}
		}
	}
}