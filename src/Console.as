/*
Console

author : Leon Ho
*/
package
{
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	
	import sound.SoundManager;
	
	public class Console extends Sprite implements IUpdateable
	{
		private const consoleHotKey:uint = 192;
		private var consoleToggle:Boolean = false;
		
		private var input:TextField = new TextField;
		private var output:TextField = new TextField;
		
		private var stats:TextField = new TextField;
		
		private var game:Game;
		
		private var commands:Dictionary = new Dictionary;
		
		private var history:Vector.<String> = new Vector.<String>;
		private const MAX_HISTORY:uint = 5;
		private var historyIndex:int = 0;
		
		public function Console(game:Game)
		{
			super();
			
			this.game = game;
			
			var format:TextFormat = new TextFormat("Helvetica", 12, 0x000000, true);
			
			input.defaultTextFormat = format;
			input.type = TextFieldType.INPUT;
			input.autoSize = TextFieldAutoSize.NONE;
			input.background = true;
			input.backgroundColor = 0x696969;
			input.width = game.stage.stageWidth;
			input.height = 20;
			input.y = 68;
			addChild(input);
			
			output.defaultTextFormat = format;
			output.type = TextFieldType.DYNAMIC;
			output.autoSize = TextFieldAutoSize.NONE;
			output.background = true;
			output.backgroundColor = 0x696969;
			output.multiline = true;
			output.width = game.stage.stageWidth * 0.5;
			output.height = 68;
			output.selectable = false;
			output.mouseWheelEnabled = true;
			output.alpha = 0.8;
			addChild(output);
			
			stats.defaultTextFormat = format;
			stats.type = TextFieldType.DYNAMIC;
			stats.autoSize = TextFieldAutoSize.NONE;
			stats.background = true;
			stats.backgroundColor = 0x696969;
			stats.multiline = true;
			stats.width = game.stage.stageWidth * 0.5;
			stats.x = output.width;
			stats.height = 68;
			stats.selectable = false;
			stats.alpha = 0.8;
			addChild(stats);
			
			this.y = game.stage.stageHeight - this.height;
			
			game.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			
			// clear the console
			addCommand("clear", "clear");
			// list the available commands
			addCommand("list", "list");
			
			addCommand("tree", "tree");
			
			addCommand("play", "play");
		}
		
		public function log(str:String):void
		{
			output.appendText(str + "\n");
			output.scrollV = output.maxScrollV;
		}
		
		public function onKeyUp(e:KeyboardEvent):void
		{
			if (e.keyCode == consoleHotKey)
			{
				input.text = "";
				consoleToggle = !consoleToggle;
				if (consoleToggle)
				{
					game.addChild(this);
					// grab focus
					stage.focus = input;
				}
				else
				{
					// return focus
					stage.focus = stage;
					game.removeChild(this);
				}
			}
			else if (e.keyCode == Keyboard.UP)
			{
				if (history.length > 0)
				{
					input.text = history[historyIndex];
					input.setSelection(input.length, input.length);
					historyIndex = Math.max(historyIndex - 1, 0);
				}
			}
			else if (e.keyCode == Keyboard.DOWN)
			{
				if (history.length > 0)
				{
					if (historyIndex == history.length - 1)
					{
						input.text = "";
					}
					else
					{
						historyIndex = Math.min(historyIndex + 1, history.length - 1);
						input.text = history[historyIndex];
						input.setSelection(input.length, input.length);
					}
				}
			}
			else if (e.keyCode == Keyboard.ENTER)
			{
				var text:String = input.text.toLowerCase();
				var line:Array = text.split(" ");

				var outputText:String;
				if (commands[line[0]])
				{
					if (history.length == MAX_HISTORY)
						history.shift();
					history.push(commands[line[0]]);
					historyIndex = history.length - 1;
					
					var command:Function = this[commands[line[0]]] as Function;
					var args:Array = line.slice(1);
					outputText = processCommand(command, args);
				}
				else
				{
					outputText = "no such command";
				}
				output.appendText(outputText + "\n");
				output.scrollV = output.maxScrollV;
				input.text = "";
			}
		}
		
		public function processCommand(command:Function, args:Array):String
		{
			var out:String;
			try 
			{
				switch (args.length)
				{
					case 0:
						out = command();
						break;
					case 1:
						out = command(args[0]);
						break;
					case 2:
						out = command(args[0], args[1]);
						break;
					case 3:
						out = command(args[0], args[1], args[2]);
						break;
					case 4:
						out = command(args[0], args[1], args[2], args[3]);
						break;
					case 5:
						out = command(args[0], args[1], args[2], args[3], args[4]);
						break;
				}
			}
			catch (e:Error)
			{
				out = e.message;
			}
			return out;
		}
		
		public function addCommand(command:String, commandHandler:String):void
		{
			commands[command] = commandHandler;
		}
		
		public function update(deltaTime:Number):void
		{
			stats.text = 	"fps: " + Game.fps + "\n";
		}
		
		public function clear():String
		{
			output.text = "";
			return "";
		}
		
		public function list():String
		{
			var cmds:Array = new Array;
			for (var key:Object in commands)
			{
				cmds.push(key);
			}
			return cmds.join("\n");
		}
		
		public function tree():String
		{
			return SoundManager.sharedManager.printTree();
		}
		
		public function play(cueName:String):String
		{
			SoundManager.sharedManager.play(cueName);
			
			return "play test sound";
		}
	}
}