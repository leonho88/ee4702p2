/*
SoundManager
facade class that provides options for playback of sounds from the sound bank.
author: Leon Ho
*/
package sound
{
	import flash.events.Event;
	import flash.events.SampleDataEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	
	import sound.container.*;

	public class SoundManager
	{
		// available buses
		public static const MASTER_BUS:String = "Master";
		public static const DIALOG_BUS:String = "Dialog";
		public static const SFX_BUS:String = "SFX";
		public static const MUSIC_BUS:String = "Music";
		public static const CRITICAL_DIALOG_BUS:String = "Critical Dialog";
		public static const NON_CRTICIAL_DIALOG_BUS:String = "Non-Critical Dialog";
		public static const AMBIENCE_BUS:String = "Ambience";
		public static const WEAPONS_BUS:String = "Weapons";
		public static const NPC_RESPONSE_BUS:String = "NPCResponse";
		
		public static function get sharedManager():SoundManager
		{
			if (!_instance) _instance = new SoundManager();
			return _instance;
		}
		
		public function SoundManager()
		{
			waveBank = new WaveBank();
			soundBank = new SoundBank(waveBank);
			
			waveBank.addEventListener(Event.COMPLETE, waveBankLoaded);
			
			createBusTree();
			
			outputSound = new Sound();
			outputSound.addEventListener(SampleDataEvent.SAMPLE_DATA, onSampleData);
		}
		
		// Playback functions
		public function play(cueName:String):void
		{
			soundBank.play(cueName);
		}
		
		// SoundManager private functions
		private function onSampleData(e:SampleDataEvent):void
		{
			// Compute difference from writing and audible audio data
			if (outputSoundChannel)
				latency = e.position / 44.1 - outputSoundChannel.position;
			
			masterBus.generateAudio(outL, outR, CHUNK_LEN);
			
			for (var i:int = 0; i < CHUNK_LEN; i++)
			{
				e.data.writeFloat(outL[i]);
				e.data.writeFloat(outR[i]);
			}
		}
		
		private function createBusTree():void
		{
			// create our bus hierarchy
			masterBus = new Bus(MASTER_BUS);
			
			var dialogBus:Bus = new Bus(DIALOG_BUS);
			var sfxBus:Bus = new Bus(SFX_BUS);
			var musicBus:Bus = new Bus(MUSIC_BUS);
			masterBus.addChild(dialogBus);
			masterBus.addChild(sfxBus);
			masterBus.addChild(musicBus);
			
			/* currently unused buses
			var critDialogBus:Bus = new Bus(CRITICAL_DIALOG_BUS);
			var noncritDialogBus:Bus = new Bus(NON_CRTICIAL_DIALOG_BUS);
			dialogBus.addChild(critDialogBus);
			dialogBus.addChild(noncritDialogBus);
			
			var ambienceBus:Bus = new Bus(AMBIENCE_BUS);
			var weaponsBus:Bus = new Bus(WEAPONS_BUS);
			var npcBus:Bus = new Bus(NPC_RESPONSE_BUS);
			sfxBus.addChild(ambienceBus);
			sfxBus.addChild(weaponsBus);
			sfxBus.addChild(npcBus);
			*/
		}
		
		private function waveBankLoaded(e:Event):void
		{
			// ideally, sounds should be defined in an external file
			// it can then be loaded, and designers can edit the asset without using code
			// here we create the sounds we want to use in code
			createSounds();
			
			outputSoundChannel = outputSound.play();	
		}

		private function createSounds():void
		{
			// create all our sounds
			
			// bgm with RTPC control to be implemented
			var bgm:BlendContainer = new BlendContainer("bgm");
			soundBank.addSound(bgm);
			bgm.bus = masterBus.getChildByName(MUSIC_BUS);
			bgm.gain = 1.0;
			var bgm_ambient_track:Track = new Track(waveBank.getWave("bgm_ambient.mp3"));
			bgm.addHorizontalTrack(bgm_ambient_track, 0, 0.4);
			var bgm_tension_track:Track = new Track(waveBank.getWave("bgm_tension.mp3"));
			bgm.addHorizontalTrack(bgm_tension_track, 0.3, 0.7);
			var bgm_action_track:Track = new Track(waveBank.getWave("bgm_action.mp3"));
			bgm.addHorizontalTrack(bgm_action_track, 0.6, 1.0);
			//var bgm_stinger_track:Track = new Track(waveBank.getWave("bgm_stinger.mp3"));
			//bgm.addVerticalTrack(bgm_stinger_track, 0.55);
			
			// explosion with pitch variation to be implemented
			var sfx_explosion:RandomContainer = new RandomContainer("sfx_explosion");
			soundBank.addSound(sfx_explosion);
			sfx_explosion.bus = masterBus.getChildByName(SFX_BUS);
			sfx_explosion.gain = 3.0;
			sfx_explosion.looping = true;
			var sfx_explosion_track1:Track = new Track(waveBank.getWave("sfx_explosion_01.mp3"));
			sfx_explosion.addTrack(sfx_explosion_track1);
			var sfx_explosion_track2:Track = new Track(waveBank.getWave("sfx_explosion_02.mp3"));
			sfx_explosion.addTrack(sfx_explosion_track2);
			var sfx_explosion_track3:Track = new Track(waveBank.getWave("sfx_explosion_03.mp3"));
			sfx_explosion.addTrack(sfx_explosion_track3);
		}
		
		public function printTree():String
		{
			return printTreeRec(masterBus, 0);
		}
		
		private function printTreeRec(bus:Bus, depth:int):String
		{
			var outString:String = "";
			for (var i:int = 0; i < depth; ++i)
				outString += "\t";
			
			outString += bus.busName + "\n";
			
			for each (var child:Bus in bus.children)
				outString += printTreeRec(child, depth+1);
				
			return outString;
		}
		
		private static var _instance:SoundManager;
		private var waveBank:WaveBank;
		private var soundBank:SoundBank;
		private var masterBus:Bus;
		
		private var outputSound:Sound;
		private var outputSoundChannel:SoundChannel;
		public static const CHUNK_LEN:int = 3072;
		private var outL:Vector.<Number> = new Vector.<Number>(CHUNK_LEN);
		private var outR:Vector.<Number> = new Vector.<Number>(CHUNK_LEN);
		
		private var latency:Number = 0.0;
	}
}