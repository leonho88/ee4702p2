package sound
{
	public class SoundUtils
	{
		// normalized trapezoidal function, left shoulder from a to b, right shoulder from c to d
		public static function trapFunc(x:Number, a:Number, b:Number, c:Number, d:Number):Number
		{
			return Math.max(Math.min((x-a)/(b-a), 1, (d-x)/(d-c)), 0);
		}
		
		public static function fadeIn(x:Number, start:Number, end:Number):Number
		{
			return ((x-start)/(end-start));
		}
		
		public static function randRange(lo:int, hi:int):Number
		{
			return Math.random()*(hi-lo) + lo;
		}
	}
}