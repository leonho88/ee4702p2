package sound
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.media.Sound;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;

	public class WaveBank extends EventDispatcher
	{
		public const waveBankDirectory:String = "waves";
		
		public function WaveBank()
		{
			// find the wave directory
			var directory:File = File.applicationDirectory.resolvePath(waveBankDirectory);
			var list:Array = directory.getDirectoryListing();
			// load all the waves in our application
			numToLoad = list.length;
			var request:URLRequest = new URLRequest();
			for (var i:uint = 0; i < list.length; i++) 
			{
				var wav:Sound = new Sound();
				request.url = (list[i] as File).nativePath;
				wav.load(request);
				
				wav.addEventListener(Event.COMPLETE, onLoadComplete);
			}
		}
		
		public function getWave(wavName:String):Sound
		{
			// retrieve the sound from the cache
			if (waves[wavName]) return waves[wavName];
			else throw new Error("WaveBank Error: " + wavName + " not in wave bank");
		}
		
		public function onLoadComplete(e:Event):void
		{
			// cache the loaded sound
			var wav:Sound = e.target as Sound;
			var wavName:String = wav.url.substring(wav.url.lastIndexOf("/")+1);
			waves[wavName] = wav;
			
			// loading of all sounds complete
			if (--numToLoad == 0)
				dispatchEvent(new Event(Event.COMPLETE));
		}
		
		private var numToLoad:int = 0; 
		private var waves:Dictionary = new Dictionary();
	}
}