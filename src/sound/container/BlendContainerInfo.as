package sound.container
{
	public class BlendContainerInfo
	{
		public var horizontalPositions:Vector.<Number>;
		public var verticalPositions:Vector.<Number>;
		
		public function BlendContainerInfo(h:int, v:int)
		{
			horizontalPositions = new Vector.<Number>(h);
			verticalPositions = new Vector.<Number>(v);
		}
	}
}