package sound.container
{
	

	public class RandomContainerInfo
	{
		public var track:Track;
		public var position:Number = 0;
		
		public function RandomContainerInfo(track:Track)
		{
			this.track = track;
		}
	}
}