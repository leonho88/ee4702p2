/*
RandomContainer
this is a container that randomly picks a wave to be played
author: Leon Ho
*/
package sound.container
{
	import flash.utils.ByteArray;
	
	import sound.SoundInstance;

	public class RandomContainer extends Container
	{
		public function RandomContainer(cueName:String)
		{
			super(cueName);
		}
		
		public override function generateAudio(instance:SoundInstance, outL:Vector.<Number>, outR:Vector.<Number>, n:uint):void
		{
			var info:RandomContainerInfo = consumers[instance];
			
			var track:Track = info.track;
			var read:int = track.extract(temp, n, info.position);
			info.position += read;
			// if the current track has ended, choose a new wave
			if (read < n)
			{
				track = selectRandomTrack();
				info.track = track;
				if (looping)
					info.position = track.extract(temp, n - read, 0);
			}
			temp.position = 0;
			for (var i:int = 0; i < n; i++)
			{
				outL[i] = temp.readFloat();
				outR[i] = temp.readFloat();
			}
			temp.position = 0;
		}
		
		public override function registerConsumer(instance:SoundInstance):void
		{
			consumers[instance] = new RandomContainerInfo(selectRandomTrack());
		}
		
		public function addTrack(track:Track):void
		{
			tracks.push(track);
		}
		
		private function selectRandomTrack():Track
		{
			return tracks[int(Math.random()*tracks.length)];
		}
		
		private var temp:ByteArray = new ByteArray();
		
		private var tracks:Vector.<Track> = new Vector.<Track>();
	}
}