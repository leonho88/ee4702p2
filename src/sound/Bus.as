/*
Bus

author: Leon Ho
*/
package sound
{
	import sound.container.Container;

	public class Bus
	{
		public var busName:String;
		public var children:Vector.<Bus> = new Vector.<Bus>();
		
		public function Bus(busName:String)
		{
			this.busName = busName;
		}
		
		public function addChild(bus:Bus):void
		{
			children.push(bus);	
		}
		
		public function getChildByName(busName:String):Bus
		{
			var retVal:Bus = null;
			for each (var child:Bus in children)
			{
				if (child.busName == busName) retVal = child;
				else retVal = child.getChildByName(busName);
				
				if (retVal) break;
			}
			return retVal;
		}
		
		public function play(container:Container):void
		{
			// create a new instance of the sound for the given container, and add it to the playing sounds
			var instance:SoundInstance = new SoundInstance(container);
			soundInstances.push(instance);
		}
		
		public function kill(instance:SoundInstance):void
		{
			soundInstances.splice(soundInstances.indexOf(instance), 1);
		}
		
		public function generateAudio(outL:Vector.<Number>, outR:Vector.<Number>, n:uint):void
		{
			var sources:int = children.length + soundInstances.length;
			
			if (sources == 0) return;
			
			for (i = 0; i < n; i++)
			{
				acc_bufL[i] = 0;
				acc_bufR[i] = 0;
			}
			
			// equal power mixing over all buses and sound instances
			var mixFactor:Number = 1.0 / sources;
		
			var i:int = 0;
			for each (var child:Bus in children)
			{
				child.generateAudio(temp_bufL, temp_bufR, n);
				
				for (i = 0; i < n; i++)
				{
					acc_bufL[i] += mixFactor*temp_bufL[i];
					acc_bufR[i] += mixFactor*temp_bufR[i];
				}
			}
			for each (var soundInstance:SoundInstance in soundInstances)
			{
				soundInstance.generateAudio(temp_bufL, temp_bufR, n);
				
				for (i = 0; i < n; i++)
				{
					acc_bufL[i] += mixFactor*temp_bufL[i];
					acc_bufR[i] += mixFactor*temp_bufR[i];
				}
			}
			for (i = 0; i < n; i++)
			{
				outL[i] = acc_bufL[i] / mixFactor;
				outR[i] = acc_bufR[i] / mixFactor;
			}
		}
		
		private var soundInstances:Vector.<SoundInstance> = new Vector.<SoundInstance>();
		
		private var acc_bufL:Vector.<Number> = new Vector.<Number>(SoundManager.CHUNK_LEN);
		private var acc_bufR:Vector.<Number> = new Vector.<Number>(SoundManager.CHUNK_LEN);
		
		private var temp_bufL:Vector.<Number> = new Vector.<Number>(SoundManager.CHUNK_LEN);
		private var temp_bufR:Vector.<Number> = new Vector.<Number>(SoundManager.CHUNK_LEN);
	}
}