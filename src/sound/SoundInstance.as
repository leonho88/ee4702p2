/*
SoundInstance
an instance of a sound
this is the consumer counterpart of the producer-consumer pattern I use for Container-SoundInstance interaction
author: Leon Ho
*/
package sound
{
	import effects.PitchShifter;
	
	import flash.media.Sound;
	import flash.utils.ByteArray;
	
	import sound.container.Container;

	public class SoundInstance
	{	
		public function SoundInstance(container:Container)
		{
			this.container = container;
			
			container.registerConsumer(this);
		}
		
		public function generateAudio(outL:Vector.<Number>, outR:Vector.<Number>, n:uint):void
		{	
			container.generateAudio(this, outL, outR, n);
		}
		
		private function cleanup():void
		{
			container.unregisterConsumer(this);
			
			container.bus.kill(this);
		}
		// pointer to the container
		private var container:Container;
	}
}